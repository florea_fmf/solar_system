/* tinem minte pozitia vertical la soarec, pentru a stii unde sa afisam meniul */
var mouse_y_pos = 0;

/* chemata cand pagina este incarta pentru prima oara;
 * seteaza inaltimea (cati pixeli vertacali) meniul inregistreaza soarecu si apare;
 * nota: metoda asta nu da rezultate scontate pe firefox; aparent ff randeaza vertical
 *       inaltimea ferestrei la browser + ~15%; Daca pagina este mai inalta de cat alege
 *       ff sa randeze atunci meniul nu o sa apara unde trebuie; */
function setMenuHeightSensitivity() {
	pageHeight = document.getElementById('page').scrollHeight;
	dv = document.getElementById('div_menu_container');
	dv.style.position = "absolute";
	dv.style.width = "40px";
	dv.style.height = pageHeight + "px";
	dv.style.zIndex = "100";
}

/* pune meniul la inaltimea soarecului */
function setMenuHeight() {
	dv = document.getElementById('div_menu');
		dv.style.top = mouse_y_pos / 1.5 + "px";
}

/* returneaza pozitia verticala a soarecului */
function getMouseYPos(e) {
	mouse_y_pos = e.clientY;
}

/* conectam funcia care inregistreaza pozitia la soarec la
 * evenimentul de onmousemouve al documentului */
document.onmousemove = getMouseYPos;
