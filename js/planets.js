/* wrapper care usureaza crearea de modele 3d ale planetelor;
 * creeaza o sfera (si cerc la planetele cu inele), o textureaza
 * si returneaza obiectul spre a putea fi folosit in WebGL */

var PLANETS = PLANETS || {}
PLANETS	= {}

/* texturile: http://planetpixelemporium.com/ 
 * nota: din motive de securite nu am putut folosi imaginile direct. Acestea au
 * fost convertite in variabile de tip imagini (Image) in javascript (se afla
 * in js/texturi). Imaginile au fost trecute prin base64 pentru a deveni text si
 * rexultatul a fost atribuit la campul "src" al variabilei */
/* creaza soarele */
PLANETS.createSun = function() {
	/* creaza o sfera */
	var geometry = new THREE.SphereGeometry(0.5, 32, 32)
	
	/* incarca imaginea pentru textura */
	var texture	= new THREE.Texture(sunmap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	/* creaza un material ce poate fi folosit ca textura;
	 * se modeleaza pe sfera */
	var material = new THREE.MeshPhongMaterial({
		map: texture,
		bumpMap: texture,
		bumpScale: 0.05,
	})
	
	/* creaza obiectul final din sfera+textura */
	var mesh = new THREE.Mesh(geometry, material)
	
	/* returneaza obiectul */
	return mesh	
}

/* acceasi poveste ca mai sus */
PLANETS.createMercury = function() {
	var geometry = new THREE.SphereGeometry(0.5, 32, 32)
	
	var texture	= new THREE.Texture(mercurymap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	var bump = new THREE.Texture(mercurybump)
	bump.wrapS = THREE.RepeatWrapping
	bump.wrapT = THREE.RepeatWrapping
	bump.needsUpdate = true
	
	var material = new THREE.MeshPhongMaterial({
		map: texture,
		bumpMap: bump,
		bumpScale: 0.005,
	})
	
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}

PLANETS.createVenus	= function() {
	var geometry = new THREE.SphereGeometry(0.5, 32, 32)
	
	var texture	= new THREE.Texture(venusmap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	var bump = new THREE.Texture(venusbump)
	bump.wrapS = THREE.RepeatWrapping
	bump.wrapT = THREE.RepeatWrapping
	bump.needsUpdate = true
	
	var material = new THREE.MeshPhongMaterial({
		map: texture,
		bumpMap: bump,
		bumpScale: 0.005,
	})
	
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}

PLANETS.createEarth	= function() {
	var geometry = new THREE.SphereGeometry(0.5, 32, 32)
	
	var texture	= new THREE.Texture(earthmap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	var bump = new THREE.Texture(earthbump)
	bump.wrapS = THREE.RepeatWrapping
	bump.wrapT = THREE.RepeatWrapping
	bump.needsUpdate = true
	
	var spec = new THREE.Texture(earthspec)
	spec.wrapS = THREE.RepeatWrapping
	spec.wrapT = THREE.RepeatWrapping
	spec.needsUpdate = true
	
	var material = new THREE.MeshPhongMaterial({
		map: texture,
		bumpMap: bump,
		bumpScale: 0.05,
		specularMap: spec,
		specular: new THREE.Color('grey'),
	})
	
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}

PLANETS.createEarthCloud = function() {
	// create destination canvas
	var canvasResult = document.createElement('canvas')
	canvasResult.width = 1024
	canvasResult.height	= 512
	var contextResult = canvasResult.getContext('2d')		

	// load earthcloudmap
	var imageMap = new Image();
	imageMap.addEventListener("load", function() {
		
		// create dataMap ImageData for earthcloudmap
		var canvasMap = document.createElement('canvas')
		canvasMap.width	= imageMap.width
		canvasMap.height= imageMap.height
		var contextMap = canvasMap.getContext('2d')
		contextMap.drawImage(imageMap, 0, 0)
		var dataMap	= contextMap.getImageData(0, 0, canvasMap.width, canvasMap.height)

		// load earthcloudmaptrans
		var imageTrans	= new Image();
		imageTrans.addEventListener("load", function() {
			// create dataTrans ImageData for earthcloudmaptrans
			var canvasTrans	= document.createElement('canvas')
			canvasTrans.width = imageTrans.width
			canvasTrans.height = imageTrans.height
			var contextTrans = canvasTrans.getContext('2d')
			contextTrans.drawImage(imageTrans, 0, 0)
			var dataTrans = contextTrans.getImageData(0, 0, canvasTrans.width, canvasTrans.height)
			// merge dataMap + dataTrans into dataResult
			var dataResult = contextMap.createImageData(canvasMap.width, canvasMap.height)
			for(var y = 0, offset = 0; y < imageMap.height; y++) {
				for(var x = 0; x < imageMap.width; x++, offset += 4){
					dataResult.data[offset+0] = dataMap.data[offset+0]
					dataResult.data[offset+1] = dataMap.data[offset+1]
					dataResult.data[offset+2] = dataMap.data[offset+2]
					dataResult.data[offset+3] = 255 - dataTrans.data[offset+0]
				}
			}
			// update texture with result
			contextResult.putImageData(dataResult,0,0)	
			material.map.needsUpdate = true;
		})
		imageTrans.src = earthcloudmaptrans.src;
	}, false);
	imageMap.src = earthcloudmap.src;

	var geometry = new THREE.SphereGeometry(0.51, 32, 32)
	
	var material = new THREE.MeshPhongMaterial({
		map: new THREE.Texture(canvasResult),
		side: THREE.DoubleSide,
		transparent: true,
		opacity: 0.8,
	})
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}


PLANETS.createMoon = function() {
	var geometry = new THREE.SphereGeometry(0.5, 32, 32)
	
	var texture	= new THREE.Texture(moonmap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	var bump = new THREE.Texture(moonbump)
	bump.wrapS = THREE.RepeatWrapping
	bump.wrapT = THREE.RepeatWrapping
	bump.needsUpdate = true
	
	var material = new THREE.MeshPhongMaterial({
		map: texture,
		bumpMap: bump,
		bumpScale: 0.002,
	})
	
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}

PLANETS.createMars = function() {
	var geometry = new THREE.SphereGeometry(0.5, 32, 32)
	
	var texture	= new THREE.Texture(marsmap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	var bump = new THREE.Texture(marsbump)
	bump.wrapS = THREE.RepeatWrapping
	bump.wrapT = THREE.RepeatWrapping
	bump.needsUpdate = true
	
	var material = new THREE.MeshPhongMaterial({
		map: texture,
		bumpMap: bump,
		bumpScale: 0.05,
	})
	
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}

PLANETS.createJupiter = function() {
	var geometry = new THREE.SphereGeometry(0.5, 32, 32)
	
	var texture	= new THREE.Texture(jupitermap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	var material = new THREE.MeshPhongMaterial({
		map: texture,
		bumpMap: texture,
		bumpScale: 0.02,
	})
	
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}


PLANETS.createSaturn = function() {
	var geometry = new THREE.SphereGeometry(0.5, 32, 32)
	
	var texture	= new THREE.Texture(saturnmap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	var material = new THREE.MeshPhongMaterial({
		map: texture,
		bumpMap: texture,
		bumpScale: 0.05,
	})
	
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}


PLANETS.createSaturnRing = function(){
	var canvasResult = document.createElement('canvas')
	canvasResult.width = 915
	canvasResult.height	= 64
	var contextResult = canvasResult.getContext('2d')	

	var imageMap = new Image();
	imageMap.addEventListener("load", function() {
		
		var canvasMap = document.createElement('canvas')
		canvasMap.width	= imageMap.width
		canvasMap.height= imageMap.height
		var contextMap = canvasMap.getContext('2d')
		contextMap.drawImage(imageMap, 0, 0)
		var dataMap	= contextMap.getImageData(0, 0, canvasMap.width, canvasMap.height)


		var imageTrans = new Image();
		imageTrans.addEventListener("load", function() {

			var canvasTrans	= document.createElement('canvas')
			canvasTrans.width = imageTrans.width
			canvasTrans.height = imageTrans.height
			var contextTrans = canvasTrans.getContext('2d')
			contextTrans.drawImage(imageTrans, 0, 0)
			var dataTrans = contextTrans.getImageData(0, 0, canvasTrans.width, canvasTrans.height)

			var dataResult = contextMap.createImageData(canvasResult.width, canvasResult.height)
			for(var y = 0, offset = 0; y < imageMap.height; y++) {
				for(var x = 0; x < imageMap.width; x++, offset += 4){
					dataResult.data[offset+0] = dataMap.data[offset+0]
					dataResult.data[offset+1] = dataMap.data[offset+1]
					dataResult.data[offset+2] = dataMap.data[offset+2]
					dataResult.data[offset+3] = 255 - dataTrans.data[offset+0]/4
				}
			}

			contextResult.putImageData(dataResult,0,0)	
			material.map.needsUpdate = true;
		})
		imageTrans.src = saturnringpattern.src;
	}, false);
	imageMap.src = saturnringcolor.src;
	
	var geometry = new PLANETS._RingGeometry(0.55, 0.75, 64);
	
	var material = new THREE.MeshPhongMaterial({
		map: new THREE.Texture(canvasResult),
		side: THREE.DoubleSide,
		transparent: true,
		opacity: 0.8,
	})
	
	var mesh = new THREE.Mesh(geometry, material)
	mesh.lookAt(new THREE.Vector3(0.5,-4,1))
	
	return mesh	
}


PLANETS.createUranus = function() {
	var geometry = new THREE.SphereGeometry(0.5, 32, 32)
	
	var texture	= new THREE.Texture(uranusmap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	var material = new THREE.MeshPhongMaterial({
		map: texture,
		bumpMap: texture,
		bumpScale: 0.05,
	})
	
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}

PLANETS.createUranusRing = function() {
	var canvasResult = document.createElement('canvas')
	canvasResult.width = 1024
	canvasResult.height = 72
	var contextResult = canvasResult.getContext('2d')	

	var imageMap = new Image();
	imageMap.addEventListener("load", function() {
		
		var canvasMap = document.createElement('canvas')
		canvasMap.width	= imageMap.width
		canvasMap.height = imageMap.height
		var contextMap = canvasMap.getContext('2d')
		contextMap.drawImage(imageMap, 0, 0)
		var dataMap	= contextMap.getImageData(0, 0, canvasMap.width, canvasMap.height)

		var imageTrans = new Image();
		imageTrans.addEventListener("load", function() {
			var canvasTrans = document.createElement('canvas')
			canvasTrans.width = imageTrans.width
			canvasTrans.height = imageTrans.height
			var contextTrans = canvasTrans.getContext('2d')
			contextTrans.drawImage(imageTrans, 0, 0)
			var dataTrans = contextTrans.getImageData(0, 0, canvasTrans.width, canvasTrans.height)
			var dataResult = contextMap.createImageData(canvasResult.width, canvasResult.height)
			for(var y = 0, offset = 0; y < imageMap.height; y++) {
				for(var x = 0; x < imageMap.width; x++, offset += 4){
					dataResult.data[offset+0] = dataMap.data[offset+0]
					dataResult.data[offset+1] = dataMap.data[offset+1]
					dataResult.data[offset+2] = dataMap.data[offset+2]
					dataResult.data[offset+3] = 255 - dataTrans.data[offset+0]/2
				}
			}
			contextResult.putImageData(dataResult,0,0)	
			material.map.needsUpdate = true;
		})
		imageTrans.src = uranusringtrans.src;
	}, false);
	imageMap.src = uranusringcolor.src;
	
	var geometry = new PLANETS._RingGeometry(0.55, 0.75, 64);
	
	var material = new THREE.MeshPhongMaterial({
		map: new THREE.Texture(canvasResult),
		side: THREE.DoubleSide,
		transparent: true,
		opacity: 0.8,
	})
	
	var mesh = new THREE.Mesh(geometry, material)
	mesh.lookAt(new THREE.Vector3(0.5,-4,1))
	
	return mesh	
}


PLANETS.createNeptune = function() {
	var geometry = new THREE.SphereGeometry(0.5, 32, 32)
	
	var texture	= new THREE.Texture(neptunemap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	var material = new THREE.MeshPhongMaterial({
		map: texture,
		bumpMap: texture,
		bumpScale: 0.05,
	})
	
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}


PLANETS.createPluto	= function() {
	var geometry = new THREE.SphereGeometry(0.5, 32, 32)
	
	var texture	= new THREE.Texture(plutomap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	var bump = new THREE.Texture(plutobump)
	bump.wrapS = THREE.RepeatWrapping
	bump.wrapT = THREE.RepeatWrapping
	bump.needsUpdate = true
	
	var material = new THREE.MeshPhongMaterial({
		map: texture,
		bumpMap: bump,
		bumpScale: 0.005,
	})
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}

PLANETS.createStarfield	= function() {
	var texture	= new THREE.Texture(starfieldmap)
	texture.wrapS = THREE.RepeatWrapping
	texture.wrapT = THREE.RepeatWrapping
	texture.needsUpdate = true
	
	var material = new THREE.MeshBasicMaterial({
		map: texture,
		side: THREE.BackSide
	})
	
	var geometry = new THREE.SphereGeometry(100, 32, 32)
	var mesh = new THREE.Mesh(geometry, material)
	
	return mesh	
}

// change the original from three.js because i needed different UV
PLANETS._RingGeometry = function ( innerRadius, outerRadius, thetaSegments ) {

	THREE.Geometry.call(this)

	innerRadius	= innerRadius || 0
	outerRadius	= outerRadius || 50
	thetaSegments = thetaSegments || 8

	var normal = new THREE.Vector3( 0, 0, 1 )

	for(var i = 0; i < thetaSegments; i++ ) {
		var angleLo	= (i / thetaSegments) *Math.PI*2
		var angleHi	= ((i+1) / thetaSegments) *Math.PI*2

		var vertex1	= new THREE.Vector3(innerRadius * Math.cos(angleLo), innerRadius * Math.sin(angleLo), 0);
		var vertex2	= new THREE.Vector3(outerRadius * Math.cos(angleLo), outerRadius * Math.sin(angleLo), 0);
		var vertex3	= new THREE.Vector3(innerRadius * Math.cos(angleHi), innerRadius * Math.sin(angleHi), 0);
		var vertex4	= new THREE.Vector3(outerRadius * Math.cos(angleHi), outerRadius * Math.sin(angleHi), 0);

		this.vertices.push( vertex1 );
		this.vertices.push( vertex2 );
		this.vertices.push( vertex3 );
		this.vertices.push( vertex4 );
		

		var vertexIdx = i * 4;

		// Create the first triangle
		var face = new THREE.Face3(vertexIdx + 0, vertexIdx + 1, vertexIdx + 2, normal);
		var uvs = []

		var uv = new THREE.Vector2(0, 0)
		uvs.push(uv)
		var uv = new THREE.Vector2(1, 0)
		uvs.push(uv)
		var uv = new THREE.Vector2(0, 1)
		uvs.push(uv)

		this.faces.push(face);
		this.faceVertexUvs[0].push(uvs);

		// Create the second triangle
		var face = new THREE.Face3(vertexIdx + 2, vertexIdx + 1, vertexIdx + 3, normal);
		var uvs = []

		var uv = new THREE.Vector2(0, 1)
		uvs.push(uv)
		var uv = new THREE.Vector2(1, 0)
		uvs.push(uv)
		var uv = new THREE.Vector2(1, 1)
		uvs.push(uv)

		this.faces.push(face);
		this.faceVertexUvs[0].push(uvs);
	}

	this.computeCentroids();
	this.computeFaceNormals();

	this.boundingSphere = new THREE.Sphere(new THREE.Vector3(), outerRadius);

};
PLANETS._RingGeometry.prototype = Object.create(THREE.Geometry.prototype);
