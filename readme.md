The solar system
================

Description
-----------
A web page with various info regarding the solar system.
Has some nice 3d models for planets, viewable if your browser
supports webgl.


License
-------
Copyright (C) 2012-2016 Florea Marius Florin

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented: you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

Florea Marius Florin, florea.fmf@gmail.com
